# Vagrant VM Workstation with Minikube (Kubernetes Single Cluster)

This Workstation has installed:
- Google Chrome
- Docker (Container Technology)
- Minikube (Local Kubernetes cluster)
- Kubectl command line
- Helm (Package manager for Kubernetes)

## Requirements
- [Oracle Virtualbox](https://www.virtualbox.org/)
- [Vagrant](https://www.vagrantup.com/)
- Minimum 16 GB RAM

## First create VM
`vagrant up`

if your keyboard is LATAM open a terminal and write: `. .profile`

## Start minikube
Open a terminal and execurte the nexts steps:
1. Create and start cluster: `minikube start`
2. Check cluster info: `kubectl cluster-info`
3. Open Dashboard cluster: `minikube dashboard`



